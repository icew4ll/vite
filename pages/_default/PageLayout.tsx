import React, {useEffect} from "react";
import logo from "./logo.svg";
import "./PageLayout.css";
import "prismjs/themes/prism-tomorrow.css"

export { PageLayout };

type Children = React.ReactNode;

function PageLayout({ children }: { children: Children }) {
  return (
    <React.StrictMode>
      <Layout>
        <Sidebar>
          <Logo />
          <a className="navitem" href="/">Home</a>
          <a className="navitem" href="/about">About</a>
          <a className="navitem" href="/test">Test</a>
        </Sidebar>
        <Content>{children}</Content>
      </Layout>
    </React.StrictMode>
  );
}

function Layout({ children }: { children: Children }) {
  return (
    <div
      className="bg-gray-900"
    >
      {children}
    </div>
  );
}

function Sidebar({ children }: { children: Children }) {
  return (
    <div
    >
      {children}
    </div>
  );
}

function Content({ children }: { children: Children }) {
  return (
    <div
    >
      {children}
    </div>
  );
}

function Logo() {
  return (
    <div
    >
      <a href="/">
        <img src={logo} height={64} width={64} alt="logo" />
      </a>
    </div>
  );
}
