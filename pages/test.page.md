# test

import { Counter } from "./components/Counter.tsx"

This text is written in Markdown.

MDX allows Rich React components to be used directly in Markdown: 

<Counter/>

## git

### branches

```bash
# create branch and checkout
br=tail
git checkout -b tail

# merge branch with master
git checkout master
br=tail
git merge $br
```

[Git Tutorial 6: Branches (Create, Merge, Delete a branch)](https://www.youtube.com/watch?v=sgzkY5vFKQQ)

## tailwindcss

```sh
yarn add vite-plugin-windicss windicss
```

[windicss](https://windicss.org/integrations/vite.html)
[tailwind-css](https://github.com/brillout/vite-plugin-ssr#tailwind-css)
