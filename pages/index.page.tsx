import React from "react";
import { Counter } from "./components/Counter";

export { Page };

function Page() {
  return (
    <>
      <h1 className="bg-gray-700">Welcome</h1>
      This page is:
      <ul>
        <li>Rendered to HTML.</li>
        <li>
          Interactive. <Counter />
        </li>
      </ul>
    </>
  );
}
