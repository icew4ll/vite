import reactRefresh from "@vitejs/plugin-react-refresh";
import ssr from "vite-plugin-ssr/plugin";
import { UserConfig } from "vite";
import mdx from 'vite-plugin-mdx'
import WindiCSS from "vite-plugin-windicss"

// `options` are passed to `@mdx-js/mdx`
const options = {
  // See https://mdxjs.com/advanced/plugins
  remarkPlugins: [
    require('remark-prism'),
  ],
  rehypePlugins: [],
}

const config: UserConfig = {
  plugins: [
    reactRefresh(),
    ssr(),
    mdx(options),
    WindiCSS({
      scan: {
        // By default only `src/` is scanned
        dirs: ["pages"],
        // You only have to specify the file extensions you actually use.
        fileExtensions: ["vue", "js", "ts", "jsx", "tsx", "html", "pug"]
      }
    })
  ]
};

export default config;
